#!/bin/sh -e

KEY_FILE="/root/.ssh/id_rsa"
KEY_SIZE="4096"
CRONTAB_FILE="/etc/crontabs/root"

# Generate SSH key
if [ ! -f /root/.ssh/id_rsa ]; then
	ssh-keygen -q -b "$KEY_SIZE" -t rsa -N "" -f "$KEY_FILE"
fi

# Add borgmatic crontab entry
cat <<EOF >/etc/crontabs/root
${CRONTAB_TIME:+$CRONTAB_TIME borgmatic -I -e repokey && borgmatic}
EOF

exec "$@"
