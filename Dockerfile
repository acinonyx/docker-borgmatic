# Ansible Docker image
#
# Copyright (C) 2019-2021 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

ARG PYTHON_IMAGE_TAG=alpine3.13
FROM python:${PYTHON_IMAGE_TAG}
MAINTAINER LSF operations team <ops@libre.space>

ARG BORGMATIC_VERSION
ARG BORGBACKUP_VERSION

RUN apk upgrade --no-cache \
	&& apk add --no-cache \
		openssh-keygen \
		openssh-client \
		mariadb-client \
		postgresql-client \
		borgbackup=${BORGBACKUP_VERSION}
RUN pip install --no-cache-dir borgmatic==${BORGMATIC_VERSION}

COPY docker-entrypoint.sh /usr/local/bin/

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["/usr/sbin/crond", "-f", "-L", "/dev/stdout"]
